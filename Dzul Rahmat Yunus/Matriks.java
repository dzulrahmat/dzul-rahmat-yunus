package PP1;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Matriks {
	static int[][] Matrix1 = new int [2][2];
	static int[][] Matrix2 = new int [2][2];
	static int[][] MatrixSum = new int [2][2];
	
	public static void Menu (){
		System.out.println("Menu");
		System.out.println("1. Addition");
		System.out.println("2. Reduction");
		System.out.println("3. Multiplication");
		System.out.println("4. Determinant");
		System.out.println("0. Exit");
	}
	public static int Matrixinput(){
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String InputNumber = null;
		try {
			InputNumber = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println ("Please Enter Numbers Only");
		}
		int data = Integer.parseInt(InputNumber);
		return data;
		
	}
	public static void ReadMatrix() {
		System.out.println("Enter Data Matrix 1 :");
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				System.out.print("Matriks ["+(row+1)+"]["+(coloumn+1)+"] : ");
				Matrix1[row][coloumn] = Matrixinput();
			}
		}
		System.out.println("Enter Data Matrix 2 :");
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				System.out.print("Matriks ["+(row+1)+"]["+(coloumn+1)+"] : ");
				Matrix2[row][coloumn] = Matrixinput();
			}
		}
	}
	public static void MatrixAdd(){
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				MatrixSum[row][coloumn] = Matrix1[row][coloumn] + Matrix2[row][coloumn];
			}
		}
	}
	public static void MatrixRed(){
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				MatrixSum[row][coloumn] = Matrix1[row][coloumn] - Matrix2[row][coloumn];
			}
		}
	}
	public static void MatrixMult(){
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				MatrixSum[0][0]=Matrix1[0][0]*Matrix2[0][0]+Matrix1[0][1]*Matrix2[1][0];
				MatrixSum[0][1]=Matrix1[0][0]*Matrix2[0][1]+Matrix1[0][1]*Matrix2[1][1];
				MatrixSum[1][0]=Matrix1[1][0]*Matrix2[0][0]+Matrix1[1][1]*Matrix2[1][0];
				MatrixSum[1][1]=Matrix1[1][0]*Matrix2[0][1]+Matrix1[1][1]*Matrix2[1][1];
			}
		}
	}
	public static void MatrixDeterminant(){
		System.out.println("Matriks 1");
		System.out.println();
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				System.out.print(Matrix1[row][coloumn] + " ");
			}
			System.out.println("");
		}
		int determinant1 = (Matrix1[0][0]*Matrix1[1][1])-(Matrix1[1][0]*Matrix1[0][1]);
		System.out.println("Matrix 1 Determinant Is " + determinant1);
		if (determinant1 == 0) {
			System.out.println("This is a reflection matrix");
		}
		else {
			System.out.println("This is NOT a reflection matrix");
		}
		System.out.println();
		System.out.println("Matrix 2");
		System.out.println();
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				System.out.print(Matrix2[row][coloumn] + " ");
			}
			System.out.println("");
		}
		int determinant2 = (Matrix2[0][0]*Matrix2[1][1])-(Matrix2[1][0]*Matrix2[0][1]);
		System.out.println("Matrix 2 Determinant Is " + determinant2);
		if (determinant2 == 0) {
			System.out.println("This is a reflection matrix");
		}
		else {
			System.out.println("This is NOT a reflection matrix");
		}
	}
	public static void Sumaddition(){
		System.out.println("Result: ");
		for (int row = 0; row < 2; row++){
			for (int coloumn = 0; coloumn < 2; coloumn++){
				System.out.print(MatrixSum[row][coloumn] + " ");
			}
			System.out.println("");
		}
	}
	public static void main(String[] args) {
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
		String InputChoice = null;
		int choice = 0;
		do {
			System.out.println();
			Menu();
			System.out.println("Enter Your Choice");
			try {
                InputChoice = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(InputChoice);
                    if (choice > 0 && choice == 1) {
                        ReadMatrix();
						System.out.println();
						MatrixAdd();
						System.out.println();
						Sumaddition();
                    }
                    else if (choice > 0 && choice == 2) {
						ReadMatrix();
						System.out.println();
						MatrixRed();
						System.out.println();
						Sumaddition();
                    }
					else if (choice > 0 && choice == 3) {
						ReadMatrix();
						System.out.println();
						MatrixMult();
						System.out.println();
						Sumaddition();
                    }
					else if (choice > 0 && choice == 4) {
						ReadMatrix();
						System.out.println();
						MatrixDeterminant();
                    }
                    else {
                        System.out.println("Thank You");
                    }
                }
                catch(NumberFormatException e) {
                    System.out.println("Error Input");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }
		}
		while (choice > 0);
	}
	
	
}