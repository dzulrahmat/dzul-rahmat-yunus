package PP1;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Geometry_TwoDimention {
	public static void showMenu() {
		System.out.println("Menu (Input Number Only) : ");
		System.out.println("===========================");
		System.out.println("1. Square");
		System.out.println("2. Rectangle");
		System.out.println("3. Equilateral Triangle");
		System.out.println("4. Right Triangle");
		System.out.println("5. Trapezoid");
		System.out.println("6. Circle");
		System.out.println("7. Parallelogram");
		System.out.println("8. Kite");
		System.out.println("0. Exit");
	}
	public static void Square(){
		System.out.println("Enter The Side Length");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputData = null;
		try {
			inputData = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
		}
		try {
			float side = Float.parseFloat(inputData);
			float wide = side * side;
			double around = 4 * side;
			System.out.println("Square Area"+ wide);
			System.out.println("Square Circumference" +around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
		}
	public static void Rectangle(){
		System.out.println("Enter The Side Length");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataLength = null;
		try {
			inputDataLength = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
	
		System.out.println("Enter The Width Length");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataWidth = null;
		try {
			inputDataWidth = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
	
		try {
			float length = Float.parseFloat(inputDataLength);
			float width =  Float.parseFloat(inputDataWidth);
			float wide = length * width;
			double around = (2*length)+(2*width);
			System.out.println("Rectangle Area" + wide);
			System.out.println("Rectangle Circumference" + around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
	}
	public static void EquilateralTriangle(){
		System.out.println("Enter The Triangle Side Long");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataLong = null;
		try {
			inputDataLong = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
		System.out.println("Enter The High Length");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataHigh = null;
		try {
			inputDataHigh = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
	
		try {
			float side = Float.parseFloat(inputDataLong);
			float high = Float.parseFloat(inputDataHigh);
			float wide = (side * high)/2;
			double around = 3*side;
			System.out.println("Equiteral Triangle Area"+wide);
			System.out.println("Equiteral Triangle Circumference" +around);

		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
	}
	public static void RightTriangle(){
		System.out.println("Enter The A Side Long (High)");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataA = null;
		try {
			inputDataA = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
		System.out.println("Enter The B Length (flat side)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataB = null;
		try {
			inputDataB = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The C Length (tilt side)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataC = null;
		try {
			inputDataC = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		try {
			float High = Float.parseFloat (inputDataA);
			float side = Float.parseFloat (inputDataB);
			float tilt = Float.parseFloat (inputDataC);
			float wide = (side * High)/2;
			double around = 3*side;
			System.out.println("Right Triangle Area"+wide);
			System.out.println("Right Triangle Circumference" +around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
	}
	public static void Trapezoid(){
		System.out.println("Enter The A Length (Tilt Side 1)");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataA = null;
		try {
			inputDataA = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
		System.out.println("Enter The B Length (Top)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataB = null;
		try {
			inputDataB = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The C Length (Bottom)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataC = null;
		try {
			inputDataC = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The D Length (Tilt side 2)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataD = null;
		try {
			inputDataD = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The High Length ");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataHigh = null;
		try {
			try {
				inputDataHigh = bufferedReader.readLine();
			}
			catch (IOException error){
				System.out.println("Error Input"+error.getMessage());	
			}
		
			float High = Float.parseFloat (inputDataHigh);
			float tiltSide1 = Float.parseFloat (inputDataA);
			float flatSide1 = Float.parseFloat (inputDataB);
			float flatSide2 = Float.parseFloat (inputDataC);
			float tiltSide2 = Float.parseFloat (inputDataD);
			double wide = (flatSide1+flatSide2)*(0.5 * High);
			double around = flatSide1+flatSide2+tiltSide1+tiltSide2;
			System.out.println("Trapezoid Area"+wide);
			System.out.println("Trapezoid Circumference" +around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
	}
	public static void Circle(){
		System.out.println("Enter The Radius Length");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputData = null;
		try {
			inputData = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
		}
		try {
			float radius = Float.parseFloat(inputData);
			double wide = Math.PI * Math.pow(radius, 2);
			double around = 2 * Math.PI *radius;
			System.out.println("Circle Area"+ wide);
			System.out.println("Circle Circumference" +around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
		}
	public static void Parallelogram(){
		System.out.println("Enter The A Length (Tilt Side 1)");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataA = null;
		try {
			inputDataA = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
		System.out.println("Enter The B Length (Top)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataB = null;
		try {
			inputDataB = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The C Length (Bottom)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataC = null;
		try {
			inputDataC = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The D Length (Tilt side 2)");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataD = null;
		try {
			inputDataD = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The High Length ");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataHigh = null;
		try {
			try {
				inputDataHigh = bufferedReader.readLine();
			}
			catch (IOException error){
				System.out.println("Error Input"+error.getMessage());	
			}
		
			float High = Float.parseFloat (inputDataHigh);
			float tiltSide1 = Float.parseFloat (inputDataA);
			float flatSide1 = Float.parseFloat (inputDataB);
			float flatSide2 = Float.parseFloat (inputDataC);
			float tiltSide2 = Float.parseFloat (inputDataD);
			float wide = flatSide2*High;
			double around = flatSide1+flatSide2+tiltSide1+tiltSide2;
			System.out.println("Parallelogram Area"+wide);
			System.out.println("Parallelogram Circumference" +around);
		}
		catch (NumberFormatException e){
			System.out.println("Wrong Input");
		}
	}
	
	public static void Kite(){
		System.out.println("Enter The A Length");
		BufferedReader bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataA = null;
		try {
			inputDataA = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());
	}
		System.out.println("Enter The B Length");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataB = null;
		try {
			inputDataB = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The C Length");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataC = null;
		try {
			inputDataC = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The D Length");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataD = null;
		try {
			inputDataD = bufferedReader.readLine();
		}
		catch (IOException error){
			System.out.println("Error Input"+error.getMessage());	
		}
		System.out.println("Enter The First Diagonal Length ");
		bufferedReader = new BufferedReader (new InputStreamReader (System.in));
		String inputDataFirst = null;
		try {
				inputDataFirst = bufferedReader.readLine();
			}
			catch (IOException error){
				System.out.println("Error Input"+error.getMessage());	
			}
			System.out.println("Enter The Second Diagonal Length ");
			bufferedReader = new BufferedReader (new InputStreamReader (System.in));
			String inputDataSec = null;
			try {
				try {
					inputDataSec = bufferedReader.readLine();
				}
				catch (IOException error){
					System.out.println("Error Input"+error.getMessage());	
					}

				float First = Float.parseFloat (inputDataFirst);
				float Second = Float.parseFloat (inputDataSec);
				float A = Float.parseFloat (inputDataA);
				float B = Float.parseFloat (inputDataB);
				float C = Float.parseFloat (inputDataC);
				float D = Float.parseFloat (inputDataD);
				float wide = (A * B)/2;
				double around = A + B + C + D ;
				System.out.println("Kite Area"+wide);
				System.out.println("Kite Circumference" +around);
			}
			catch (NumberFormatException e){
				System.out.println("Wrong Input");
		}
		}


		public static void main (String[]args){
			BufferedReader bufferedReader = new BufferedReader (new InputStreamReader(System.in));
			String inputData = null;
			int choice = 0;
			do {
				showMenu ();
				System.out.println("Enter Your Choice : ");
				try {
					inputData = bufferedReader.readLine();
					try {
						choice = Integer.parseInt(inputData);
						if (choice >0 && choice ==1){
							Square();
						}
						else if (choice >0 && choice ==2){
							Rectangle();
						}
						else if (choice > 0 && choice ==3){
							EquilateralTriangle();
						}
						else if (choice >0 && choice ==4){
							RightTriangle();
						}
						else if (choice >0 && choice ==5){
							Trapezoid();
						}
						else if (choice >0 && choice ==6){
							Circle();
						}
						else if (choice >0 && choice ==7){
							Parallelogram();
						}
						else if (choice > 0 && choice ==8){
							Kite();
						}
						else {
							System.out.println("++Thank you++");
						}
					}
					catch (NumberFormatException e){
						System.out.println("Error Input");
					}
					
				}
				catch (IOException error){
					System.out.println("Error Input"+error.getMessage());
			}
		}while (choice >0);
}
}


			
		